package com.example.calculadora

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat

class CalculadoraActivity : AppCompatActivity() {

    private lateinit var txtNum1: EditText
    private lateinit var txtNum2: EditText

    private lateinit var lblUsuario: TextView
    private lateinit var lblResultado: TextView

    private lateinit var btnSumar: Button
    private lateinit var btnRestar: Button
    private lateinit var btnMultiplicar: Button
    private lateinit var btnDividir: Button

    private lateinit var btnLimpiar:Button
    private lateinit var btnRegresar:Button

    private lateinit var calcu:Calculadora

    var opcion = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_calculadora)
        iniciarCompontentes()
        eventosClic()

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
    }

    public fun valiEmpty():Boolean{
        if(!txtNum1.text.toString().contentEquals("") ||
            !txtNum2.text.toString().contentEquals("")){
            return true
        }
        else{
            return false
        }
    }

    public fun operaciones():Float{
        var num1:Float=0f
        var num2:Float=0f
        var res:Float=0f

        if(valiEmpty()){
            num1 = txtNum1.text.toString().toFloat()
            num2 = txtNum2.text.toString().toFloat()
            calcu = Calculadora(num1,num2)
            when(opcion){
                1 ->{res=calcu.sumar()}
                2 ->{res=calcu.restar()}
                3 ->{res=calcu.multiplicar()}
                4 ->{res=calcu.dividir()}
            }

        }
        else{
            Toast.makeText(this,"Faltaron datos por capturar",Toast.LENGTH_SHORT).show()
        }
        return res
    }

    public fun iniciarCompontentes(){
        txtNum1 = findViewById(R.id.txtNum1)
        txtNum2 = findViewById(R.id.txtNum2)
        lblUsuario = findViewById(R.id.lblUsuario)
        lblResultado = findViewById(R.id.lblResultado)

        btnSumar = findViewById(R.id.btnSumar)
        btnRestar = findViewById(R.id.btnRestar)
        btnMultiplicar = findViewById(R.id.btnMultiplicar)
        btnDividir = findViewById(R.id.btnDividir)

        btnLimpiar = findViewById(R.id.btnLimpiar)
        btnRegresar = findViewById(R.id.btnRegresar)


        val datos = intent.extras
        lblUsuario.text = datos?.getString("nombre")

    }

    public fun eventosClic(){
        btnSumar.setOnClickListener(View.OnClickListener {
            opcion = 1
            lblResultado.text = "Resultado: " + operaciones().toString()

        })

        btnRestar.setOnClickListener(View.OnClickListener {
            opcion = 2
            lblResultado.text = "Resultado: " +operaciones().toString()

        })

        btnMultiplicar.setOnClickListener(View.OnClickListener {
            opcion = 3
            lblResultado.text = "Resultado: " +operaciones().toString()

        })

        btnDividir.setOnClickListener(View.OnClickListener {
            opcion = 4
            if(this.txtNum2.text.toString().toFloat() == 0f){
                lblResultado.text = "No es posible dividir sobre 0"
            }
            else{
                lblResultado.text = "Resultado: " + operaciones().toString()
            }


        })

        btnLimpiar.setOnClickListener(View.OnClickListener {
            lblResultado.text = "Resultado: "

            txtNum2.text.clear()
            txtNum1.text.clear()

        })

        btnRegresar.setOnClickListener(View.OnClickListener {
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Calculadora")
            builder.setMessage("¿ Deseas cerrar la app ?")
            builder.setPositiveButton(android.R.string.yes){
                dialog, witch -> this.finish()
            }
            builder.setNegativeButton(android.R.string.no){
                dialog, witch ->
            }
            builder.show()
        })
    }


}