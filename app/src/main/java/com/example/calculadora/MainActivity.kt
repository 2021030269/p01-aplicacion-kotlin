package com.example.calculadora

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat

class MainActivity : AppCompatActivity() {
    private lateinit var txtNombre: EditText
    private lateinit var txtPassword: EditText
    private lateinit var btnIngresar: Button
    private lateinit var btnSalir: Button



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_main)
        iniciarCompontentes()
        eventoClic()

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
    }

    fun valiEmpty(): Boolean {
        if(txtNombre.text.contentEquals("") ||
            txtPassword.text.contentEquals("")){
            return true
        }
        else{
            return false
        }
    }

    public fun iniciarCompontentes(){
        txtNombre = findViewById(R.id.txtUsuario)
        txtPassword = findViewById(R.id.txtPassword)
        btnIngresar = findViewById(R.id.btnIngresar)
        btnSalir = findViewById(R.id.btnSalir)
    }

    public fun eventoClic(){
        this.btnIngresar.setOnClickListener(View.OnClickListener {
            var usuario: String = "lobatos"
            var contraseña: String = "789"
            var nombre: String = "Jose Lopez"

            if (valiEmpty()){
                Toast.makeText(this, "Llene los campos nombre y contraseña", Toast.LENGTH_SHORT).show()
            }
            else{
                if(txtNombre.text.toString().equals(usuario) && txtPassword.text.toString().equals(contraseña)){
                    val intent = Intent(applicationContext, CalculadoraActivity::class.java)
                    intent.putExtra("nombre", nombre.toString())
                    startActivity(intent)
                }
                else{
                    Toast.makeText(this, "Usuario o Contraseña INCORRECTOS", Toast.LENGTH_SHORT).show()
                }
            }


        })

        btnSalir.setOnClickListener(View.OnClickListener {
            finish()
        })
    }


}